# Plain

**Дедлайн: 31 марта 23:59**

Отлично, мы проделали уже длинный путь и приближаемся к реализации RT-индекса.
Теперь мы реализуем disk-сегмент, который будет немутабельным. Разумно предположить, что
дисковый сегмент может получаться двумя способами:
1. В процессе индексации. 
2. RAM-индекс может превратиться в диск сегмент.

В этом задании мы будем реализовывать первый подход, а второй будет в задаче с RT-индексом. Также рекомендуется перед
тем как писать код ознакомиться с задачей `compression`, т.к. эта задача очень связана с этой.

Индексацией занимается программа indexer, которая располагается в папке `lib/common/indexer`.
Перед тем как индексировать подумайте где хранить созданные дисковые сегменты?

Следующий этап заключается в том как хранить же сам индекс. С одной стороны в голову приходит естественный формат
рассказанный на лекции:

Давайте отсортируем слова в лексиграфическом порядке и выпишим хиты в лексиграфическом порядке:
```
word_1:hit_1, ...
word_2:...
...
word_n:...
```

Перед тем как читать условие дальше подумайте: в чем проблема такого подхода? как её решить?

Проблема лежит на поверхности и заключается в том, что нам придётся искать слово при поиске полным проходом, что естественно
долго. Так как индекс немуттабельный будем использовать классический приём: в начале файла
запишем метаинформацию - каждое N-ое слово (N можно взять 1000) и оффсет в файле до этого слова. Тогда при поиске
можно сделать бинарный поиск по хедеру и стартуя с ближайшего лексиграфического слова сделать fullscan.
Даже в таком подходе можно сделать множество модификаций и улучшений. Додумайте структуру сами, учитывая задание compression.

Когда с индексацией завершено необходимо имплементировать сам индекс в `lib/server/index/disk.cpp`. Аналогично RAM сегментну
нужно сделать поиск по такому индексу. Каждый раз ходить на диск долго и неплохо бы сделать минимальное кэширование в памяти.
Какой подход с лекции про аттрибутивное хранилище будет эффективным при наименьших усилиях? Конечно, использование mmap, чтобы можно
было использовать кэш операционной системы page cache.

Затем осталось имплементировать уже несложную вещь: при загрузке `mipt-searchd` подгружать дисковый сегмент (чтобы вновь не индексировать).

Тестов на данное задание нет и все будет проверяться вручную.